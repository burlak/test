String.prototype.toBinary = function () {
	var res;

	if (/[^0^1]+/.test(this)) {
		throw 'incorrect';
	}

	res = this.split('').reverse().map(function (a, i) {
		return +a * Math.pow(2, i);
	}).reduce(function (a, b) {
		return a + b;
	});

	return +res;
};

module.exports = function calculate() {
	var numbers = Array.prototype.slice.call(arguments);

	return numbers.map(function (n) {
		return n.toBinary();
	}).reduce(function (a, b) {
		return a + b;
	});

};


