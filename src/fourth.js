var MS_IN_MINUTES = 1000 * 60;


function getHourAndMinutes (str) {
	str = str.split(':');

	return {
		m: +str[1],
		h: +str[0]
	};

}


var Calendar = function (schedules) {
	this.schedules = schedules;

	this._setupDayRange();
	this._calculateFreeTime();
};

Calendar.getUTCTime = function (time) {
	var today;

	if (typeof time === 'string') {
		time = getHourAndMinutes(time);
	}

	today = new Date();
	return new Date(today.getYear(), today.getMonth(), today.getDate(), time.h, time.m).getTime();
};

Calendar.includeTime = function (range, time) {
	var first = Calendar.getUTCTime(getHourAndMinutes(range[0])),
		second = Calendar.getUTCTime(getHourAndMinutes(range[1]));

	time =  Calendar.getUTCTime(getHourAndMinutes(time));
	return first <= time && second >= time;

};

Calendar.getRange = function (first, second) {
	var first = Calendar.getUTCTime(getHourAndMinutes(first)),
		second = Calendar.getUTCTime(getHourAndMinutes(second));

	return (second - first) / 1000 / 60;
};

Calendar.findStartTime = function (values, index) {
	var hasEmpty, result, start, list;

	list = Array.prototype.concat.apply([], values);
	index = index || 0;

	if (index > list.length - 1) {
		return null;
	}

	start = list[index];

	result = values.map(function (ranges) {

		return ranges.filter(function (range) {
			return Calendar.includeTime(range, start[0]);
		});

	});

	result.forEach(function (ranges) {

		if (!ranges.length) {
			hasEmpty = true;
		}

	});


	if (hasEmpty) {
		return Calendar.findStartTime(values, index + 1);
	}

	return start[0];
};

Calendar.prototype._setupDayRange = function () {

	this.schedules.forEach(function (ranges) {
		var first = getHourAndMinutes(ranges[0][0]),
			last = getHourAndMinutes(ranges[ranges.length - 1][1]);

		if (first.h >= 9 && first.m > 0) {
			ranges.unshift(['09:00', '09:00']);
		}

		if (last.h < 19) {
			ranges.push(['19:00', '19:00']);
		}

	});

};

Calendar.prototype._calculateFreeTime = function () {

	this.freetime = this.schedules.map(function (ranges) {
		var i,
			ret = [],
			first, second;

		for (i = 1; i < ranges.length; i ++) {
			first = Calendar.getUTCTime(getHourAndMinutes(ranges[i - 1][1]));
			second = Calendar.getUTCTime(getHourAndMinutes(ranges[i][0]));
			ret.push((second - first) / MS_IN_MINUTES);
		}

		return ret;
	});

};

Calendar.prototype.findTime = function (minutes) {
	var pretenders = [],
		start, end;

	this.schedules.forEach(function (shedules, user) {
		pretenders[user] = [];

		this.freetime[user].forEach(function (time, i) {

			if (time > minutes) {
				pretenders[user].push([shedules[i][1], shedules[i + 1][0]]);
			}

		});

	}.bind(this));

	if (!pretenders.length) {
		return null;
	}

	start = Calendar.findStartTime(pretenders);

	if (!start) {
		return null;
	}

	end = new Date(Calendar.getUTCTime(getHourAndMinutes(start)) + minutes * MS_IN_MINUTES);

	return [start, end.getHours() + ':' + end.getMinutes()]
};



module.exports = Calendar;
