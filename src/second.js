var numbers = {
	'zero': 0,
	'one': 1,
	'two': 2,
	'three': 3,
	'four': 4,
	'five': 5,
	'six': 6,
	'seven': 7,
	'eight': 8,
	'nine': 9
};

var operations = {
	'plus': '+',
	'minus': '-',
	'times': '*',
	'dividedBy': '/'
};

Object.keys(operations).concat(Object.keys(numbers)).forEach(function (action) {
	//three(times(two()))

	function hasOperation(arr) {
		return arr.some(function (a) { return ['+', '-', '*', '/'].indexOf(a) !== -1  });
	}

	global[action] = function (opr) {

		if (typeof opr === 'number') {
			opr = [opr];
		}

		opr = opr || [];
		opr.unshift(numbers[action] || operations[action]);

		// проверяем что это законченное выражение и схлопываем его
		if (hasOperation(opr) && numbers[action]) {
			opr = eval(opr.join(' ')); // eval конечно не очень, но чего то лучше придумать не смог
		}

		return opr;
	};

});
