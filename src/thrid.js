/**
 * Created by i.burlak on 09.05.16.
 */

// https://davidwalsh.name/javascript-arguments
function getArgs(func) {
	var args = func.toString().match(/function\s.*?\(([^)]*)\)/)[1];

	return args.split(',').map(function(arg) {
		return arg.replace(/\/\*.*\*\//, '').trim();
	}).filter(function(arg) {
		return arg;
	});

}

/**
 * Create function with default args
 * @param {Function} fn
 * @param {Object} defaults
 */
global.defaultArguments = function (fn, defaults) {
	var ret = function () {
		var args = Array.prototype.slice.call(arguments),
			params = [];

		defaults.forEach(function (key, i) {
			params.push(args[i] || defaults[i])
		});

		return ret.original.apply(this, params);
	};

	ret.args = fn.args || getArgs(fn);
	ret.original = fn.original || fn;

	defaults = ret.args.map(function (name) {
		return defaults[name] || NaN;
	});

	return ret;
};
