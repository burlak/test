require('../src/thrid');

describe('thrid', function() {

	it ('one default param', function () {

		function add (a, b) {
			return a + b;
		}

		var	add_= defaultArguments(add, {	b: 9 });

		expect(add_(10)).toBe(19);
		expect(add_(10, 7)).toBe(17);
		expect(add_()).toBeNaN();



	});

	it ('two default param', function () {

		function add(a, b) {
			return a + b;
		}

		var add_ = defaultArguments(add, {b: 3, a: 2});

		expect(add_(10)).toBe(13);
		expect(add_()).toBe(5);
	});

	it ('curring', function () {

		function add(a, b) {
			return a + b;
		}

		var add_ = defaultArguments(add, {b: 3, a: 2});
		add_ = defaultArguments(add_, {	c: 3 });

		expect(add_(10)).toBeNaN();
		expect(add_(10, 10)).toBe(20);
	});

});



