var Calendar = require('../src/fourth');

describe('fourth', function() {

	it ('simple', function () {

		var	schedules = [
			[['09:00', '11:30'], ['13:30', '16:00'], ['16:00', '17:30'], ['17:45', '19:00']],
			[['09:15', '12:00'], ['14:00', '16:30'], ['17:00', '17:30']],
			[['11:30', '12:15'], ['15:00', '16:30'], ['17:45', '19:00']]
		];

		var cld = new Calendar(schedules);

		expect(cld.findTime(60)).toEqual(['12:15', '13:15' ]);
		expect(cld.findTime(100)).toEqual([ '12:15', '13:55' ]);
		expect(cld.findTime(200)).toBe(null);
	});


	it ('includeTime', function () {
		expect(Calendar.includeTime(['09:00', '11:30'], '10:00')).toBe(true);
		expect(Calendar.includeTime(['15:00', '16:30'], '15:10')).toBe(true);
		expect(Calendar.includeTime(['13:00', '14:30'], '15:10')).toBe(false);
	});

	it ('getRange', function () {
		expect(Calendar.getRange('09:00', '10:00')).toBe(60);
		expect(Calendar.getRange('09:30', '10:00')).toBe(30);
		expect(Calendar.getRange('09:15', '11:00')).toBe(105);
	});

	it ('findStartTime', function () {
		expect(Calendar.findStartTime(
			[ [ [ '11:30', '13:30' ] ],
				[ [ '12:00', '14:00' ], [ '17:30', '19:00' ] ],
				[ [ '09:00', '11:30' ],
				[ '12:15', '15:00' ],
				[ '16:30', '17:45' ] ] ]
		)).toBe('12:15');
	});

	it ('findStartTime', function () {
		expect(Calendar.findStartTime(
			[ [ [ '11:30', '13:30' ] ],
				[ [ '12:00', '14:00' ], [ '17:30', '19:00' ] ],
				[ [ '09:00', '11:30' ],
					[ '12:15', '15:00' ],
					[ '16:30', '17:45' ] ] ]
		)).toBe('12:15');
	});


	it ('getUTCTime', function () {
		var dt = new Date(Calendar.getUTCTime('09:00')),
			today = new Date();

		expect(dt.getDate()).toBe(today.getDate());
		expect(dt.getHours()).toBe(9);
	});

});



