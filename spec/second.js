require('../src/second');

describe('second', function() {

	it ('simple', function () {
		expect(three(times(two()))).toBe(6);
		expect(four(plus(nine()))).toBe(13);
		expect(eight(minus(three()))).toBe(5);
		expect(six(dividedBy(two()))).toBe(3);
	});

	it ('extra', function () {
		// extra
		expect(two(plus(three(times(two()))))).toBe(8);

		// 9 * (2 + 3)
		expect(nine(times(two(plus(three()))))).toBe(45);
	});

});



